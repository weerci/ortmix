﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using ortmis_s.Properties;
using ortmis_s;
using System.Windows;
using System.Diagnostics;
using ortmis_s.net;

namespace Reader
{
    public class TcpIpServer
    {
        private IPEndPoint _IpEndPoint = new IPEndPoint(IPAddress.Any, Settings.Default.TCP_PORT);
        private Socket _Listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private bool _IsRun = false;

        private Socket Client { get; set; }


        public TcpIpServer()
        {
            _Listener.Bind(_IpEndPoint);
            _Listener.Listen(10);
        }

        public async void Reseive()
        {
            try
            {
                _IsRun = true;

                await Task.Run(() =>
                {

                    Application.Current.Dispatcher.InvokeAsync(() =>
                    {
                        App.MainWin.lbMessages.Items.Add($"Ожидаем соединение через порт: {_IpEndPoint}");
                    });

                    Client = _Listener.Accept();

                    while (_IsRun)
                    {
                        byte[] bytes = new byte[256];
                        int bytesRec = Client.Receive(bytes);
                        
                        Application.Current.Dispatcher.InvokeAsync(() =>
                        {
                            App.MainWin.lbMessages.Items.Add(Encoding.ASCII.GetString(bytes));
                            //new Parse(this).Bytes(bytes);
                        });
                    }
                });
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                _IsRun = false;
            }
        }

        public async void Send(byte[] msg)
        {
            await Task.Run(() =>
            {
                Client.Send(msg);
            });
        }

    }
}
