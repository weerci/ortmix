﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ortmis_s.Properties;
using System.Security.Policy;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Windows;
using System.Threading;

namespace ortmis_s.net
{
    public class Broadcast
    {
        private bool _IsRun = false;
        private UdpClient _Listener = new UdpClient(Settings.Default.UDP_PORT);
        private IPEndPoint _GroupEP = new IPEndPoint(IPAddress.Any, Settings.Default.UDP_PORT);
        private string _DataReceived;
        private byte[] _ReceivedByteArray;

        public async void Run()
        {

            await Task.Run(() =>
            {
                _IsRun = true;

                try
                {
                    while (_IsRun)
                    {
                        _IsRun = true;

                        _ReceivedByteArray = _Listener.Receive(ref _GroupEP);
                        _DataReceived = Encoding.ASCII.GetString(_ReceivedByteArray, 0, _ReceivedByteArray.Length);

                        Application.Current.Dispatcher.InvokeAsync(() =>
                        {
                            App.MainWin.lbMessages.Items.Add($"Получен запрос broadcast от клиента: {_DataReceived}");
                        });

                        byte[] send_bytes = Encoding.ASCII.GetBytes(_DataReceived);
                        _Listener.Send(send_bytes, send_bytes.Length, new IPEndPoint(_GroupEP.Address, Settings.Default.UDP_PORT));
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
                finally
                {
                    _IsRun = false;

                }
            });
        }

    }
}
