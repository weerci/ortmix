﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ortmis_s.net
{
    /// <summary>
    /// Класс класс содержит поля в которых хранится информация о сообщении.
    /// Сообщение состоит из трех частей:
    /// 1. Информация о соединении: адрес, порт, имя
    /// 2. Заголовок сообщения: 32 бита, из них 0-29 - приветствие, 30 - тип команды, 31 - кол-во зон
    /// 3. Последующие зоны повторяются, каждая занимает по 32 бита, кол-во повторений зависит от
    ///    количества зон переданных в 31 байте.Каждая повторяющаяся зона состоит из:
    ///    1. 0-29 - имя зоны
    ///    2. 30 - имя параметра
    ///    3. 31 - значение параметра
    /// </summary>
    public class Message
    {
    }
}
