﻿using ortmis_s.net;
using Reader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ortmis_s.net
{
    public class Parse
    {
        private TcpIpServer _TcpServer;

        public Parse(TcpIpServer tcpServer)
        {
            this._TcpServer = tcpServer;
        }

        public void Bytes(byte[] message)
        {
            _TcpServer.Send(message);
        }
    }
}
