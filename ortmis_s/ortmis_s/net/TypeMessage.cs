﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ortmis_s.net
{
    /// <summary>
    /// Тип передаваемого сообщения
    /// </summary>
    public enum TypeMessage
    {

        /// <summary>
        ///  Запрос со стороны клиента серверу на синхронизацию параметров.
        ///  Отправляется клиентом при создании  TCP/IP соединения */
        /// </summary>
        GET_SYNC = 0,

        /// <summary>
        /// При изменении параметра на клиенте, тип этого сообщения посылается серверу TCP/IP. 
        /// </summary>
        PARAM = 1,

        /// <summary>
        /// Закрытие TCP/IP соединения.
        /// </summary>
        CLOSE_CONNECT = 2,

        /// <summary>
        ///  TCP/IP соединение с сервером установлено.
        /// </summary>
        TCP_CONNECTION_IS_ESTABLISHED = 3,

        /// <summary>
        /// При получении сообщения этого типа, UDP socket на стороне сервера формирует и отвечает
        /// клиету сообщением типа FOUND_SERVER. 
        /// </summary>
        LOOKING_SERVER = 4,

        /// <summary>
        /// Сервер TCP/IP найден и готов к установлению соединения. UDP socket, на стороне сервера формирует сообщение 
        /// этого типа и отправляет клиенту. 
        /// Клиент устанавливает TCP/IP соединение и прекращает broadcast рассылку. 
        /// </summary>
        SYNC = 6,

        /// <summary>
        ///  Гененрируется клиентом, если не удется соединиться с сервером. 
        /// </summary>
        FILED_TO_CONNECT = 7,

    }
}
