package com.ortsoft.ortmix.common;


public final class Settings {

    // region Broadcast

    public static final int BS_PORT = 51321;
    public static final String BS_ADDRESS = "255.255.255.255";

    // The time interval during which the client repeats the broadcast request until
    // it receives a response from the server
    public static final int BS_TIME_INTERVAL = 4000;

    //A greeting line identifying the client and server as participants in the same program
    public static final String BS_HELLO = "com.ortsoft.ortmix";

    // endregion

    // region TCP/IP

    public static final int SRV_PORT = 51322;
    public static final int BS_ANSWER = 256;

    // endregion
}
