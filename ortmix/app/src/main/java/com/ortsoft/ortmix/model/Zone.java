package com.ortsoft.ortmix.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.Arrays;
import java.util.List;

public class Zone implements Parcelable {
    private String mName;
    private int mVolume;
    private int mHeight;
    private int mMid;
    private int mLow;

    /**
     * Список зон поддерживаемых сервером
     */
    public static List<Zone> Zones = Arrays.asList(
            new Zone("Zone 1", 76, 6, 6, 9),
            new Zone("Zone 2", 10, 14,15, 1),
            new Zone("Zone 3",  5, 38, 69, 46)
    );

    public Zone() {
    }

    public Zone(String name) {
        mName = name;
    }

    public Zone(String name, int volume, int height, int mid, int low) {
        mName = name;
        mVolume = volume;
        mHeight = height;
        mMid = mid;
        mLow = low;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public int getVolume() {
        return mVolume;
    }

    public void setVolume(int mVolume) {
        this.mVolume = mVolume;
    }

    public int getHeight() {
        return mHeight;
    }

    public void setHeight(int mHeight) {
        this.mHeight = mHeight;
    }

    public int getMid() {
        return mMid;
    }

    public void setMid(int mMid) {
        this.mMid = mMid;
    }

    public int getLow() {
        return mLow;
    }

    public void setLow(int mLow) {
        this.mLow = mLow;
    }


    protected Zone(Parcel in) {
        mName = in.readString();
        mVolume = in.readInt();
        mHeight = in.readInt();
        mMid = in.readInt();
        mLow = in.readInt();
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(mName);
        dest.writeInt(mVolume);
        dest.writeInt(mHeight);
        dest.writeInt(mMid);
        dest.writeInt(mLow);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Zone> CREATOR = new Creator<Zone>() {
        @Override
        public Zone createFromParcel(Parcel in) {
            return new Zone(in);
        }

        @Override
        public Zone[] newArray(int size) {
            return new Zone[size];
        }
    };
}
