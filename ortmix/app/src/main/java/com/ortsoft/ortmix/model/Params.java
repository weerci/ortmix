package com.ortsoft.ortmix.model;

public enum Params {

    VOLUME(0),             // Громкость канала 0-100
    HIGH_FREQ(1),          // Высокие частоты 0-100
    MIDDLE_FREQ(2),        // Средние частоты
    LOW_FREQ(3),           // Низкме частоты 0-100
    STREAM(4);             // Канал подключения

    private final int value;

    private Params(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
