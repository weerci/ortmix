package com.ortsoft.ortmix.messages;

import static com.ortsoft.ortmix.common.Routine.E_TAG;

import android.util.Log;

import com.ortsoft.ortmix.common.Settings;
import com.ortsoft.ortmix.model.Zone;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Класс класс содержит поля в которых хранится информация о сообщении.
 * Сообщение состоит из частей:
 * 1. Заголовок сообщения: 32 бита, из них 0-29 - приветствие, 30 - тип команды, 31 - кол-во зон
 * 2. Последующие зоны повторяются, каждая занимает по 32 бита, кол-во повторений зависит от
 * количества зон переданных в 31 байте. Каждая повторяющаяся зона состоит из:
 * 0-28 - имя зоны, 29 - значение громкости, 30 - значение высокой частоты, 30 - средней
 * 31 - низкой
 */
public class Message {

    private String mHello = Settings.BS_HELLO;              //  0-29
    private TypeMessage mTypeMessage;                       //  30
    private byte mCountZone;                                //  31 вычисляемый по zones
    private List<Zone> mZones = new ArrayList<>();      // 32 * n

    /**
     * Возвращает поля класса в виде массива, для отправки через сокет
     */
    private byte[] mMsg;

    private Message(TypeMessage typeMessage, List<Zone> zones) {
        mTypeMessage = typeMessage;
        if (zones != null)
            mZones = zones;
    }

    private Message(byte[] msg) {
        mMsg = msg;
    }


    public String getHello() {
        return mHello;
    }

    public TypeMessage getTypeMessage() {
        return mTypeMessage;
    }

    public int getCountZones() {
        return mZones == null ?  0 : mZones.size();
    }

    public List<Zone> getZones() {
        return mZones;
    }

    public byte[] getMsg() {
        return mMsg;
    }

    /**
     * Статический метод возвращающий сообщение типа команда или информацию не требующих
     * наличия значений для параметров (закрыть соединение, соедирнение прервано и пр.)
     * */
    public static Message getCommand(TypeMessage typeMessage) {
        Message res = new Message(typeMessage, null);
        res.ParseToStream();
        return res;
    }

    /**
     * Статический метод возвращающий сообщение с информацией об изменяемых параметрах в зонах
     * */
    public static Message getParams(TypeMessage typeMessage, Zone zone) {
        Message res = new Message(typeMessage, Collections.singletonList(zone));
        res.ParseToStream();
        return res;
    }

    /**
     * Статический метод возвращающий сообщение с информацией полученной из сокета и с
     * заполненными, на основе этой информации полями
     * */
    public static Message getReceived(byte[] msg) {
        Message res = new Message(msg);
        res.ParseToField();
        return res;
    }

    /**
     * Поля класса преобразует в массив битов, в соответствии со структурой сообщения
     * */
    protected void ParseToStream() {
        try {
            ByteBuffer bb = ByteBuffer.allocate(32 + getCountZones() * 32) // 32 + n *32, где n - количество зон
                    .put(Arrays.copyOf(mHello.getBytes(StandardCharsets.US_ASCII), 30))
                    .put((byte) mTypeMessage.getValue())
                    .put((byte) getCountZones());
            for (int i = 0; i < mCountZone; i++) {
                bb.put(Arrays.copyOf(mZones.get(i).getName().getBytes(StandardCharsets.US_ASCII), 30))
                        .put((byte) mZones.get(i).getVolume())
                        .put((byte) mZones.get(i).getHeight())
                        .put((byte) mZones.get(i).getMid())
                        .put((byte) mZones.get(i).getLow());
            }
            mMsg = bb.array();
        } catch (Exception e) {
            Log.e(E_TAG, Log.getStackTraceString(e));
        }
    }

    /**
     * Задает значения для полей классов, разбирая массив byte[]
     * */
    protected void ParseToField() {
        try {
            ByteBuffer bb = ByteBuffer.wrap(mMsg);

            byte[] bHello = new byte[32];
            byte[] bTypeMessage = new byte[1];
            byte[] bCountZOne = new byte[1];

            bb.get(bHello, 0, bHello.length);
            bb.get(bTypeMessage, 0, bTypeMessage.length);
            bb.get(bCountZOne, 0, bCountZOne.length);

            mHello = new String(bHello);
            mTypeMessage = TypeMessage.values()[bTypeMessage[0]];
            mCountZone = bCountZOne[0];

            for (int i = 0; i < mCountZone; i++) {
                //0-28 - имя зоны, 29 - значение громкости, 30 - значение высокой частоты, 30 - средней
                byte[] name = new byte[29];
                byte[] volume = new byte[1];
                byte[] height = new byte[1];
                byte[] mid = new byte[1];
                byte[] low = new byte[1];

                bb.get(name, 0, name.length);
                bb.get(volume, 0, volume.length);
                bb.get(height, 0, height.length);
                bb.get(mid, 0, mid.length);
                bb.get(low, 0, low.length);
                mZones.add(new Zone(new String(name), (int)volume[0], (int)height[0], (int)mid[0], (int)low[0]));
            }

        } catch (Exception e) {
            Log.e(E_TAG, Log.getStackTraceString(e));
        }
    }

}


