package com.ortsoft.ortmix;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.slider.Slider;
import com.ortsoft.ortmix.messages.Message;
import com.ortsoft.ortmix.messages.MessageSend;
import com.ortsoft.ortmix.model.Zone;
import com.ortsoft.ortmix.net.TcpClient;

public class MainActivity extends AppCompatActivity {

    private MessageHandler mMessageHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ViewPager2 pager = findViewById(R.id.pager);
        pager.setPageTransformer(new DepthPageTransformer());
        FragmentStateAdapter pageAdapter = new ViewPagerFragmentStateAdapter(this);
        pager.setAdapter(pageAdapter);

        mMessageHandler = new MessageHandler(new MessageHandler.Receive() {
            @Override
            public void receiveMessage(Message message) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        parseMessage(message);
                    }
                });
            }
        }).run();
    }

    private void parseMessage(Message message) {
        /*switch (message.getTypeMessage()) {
            case FILED_TO_CONNECT:
                mServerState.setText("Filed on connect to " + TcpClient.INSTANCE.getConnectParam().getAddress());
                break;
            case LOOKING_SERVER:
                mServerState.setText("Server search");
                break;
            case FOUND_SERVER:
                mServerState.setText("Server found" + TcpClient.INSTANCE.getConnectParam().getAddress());
                break;
        }*/
    }
}