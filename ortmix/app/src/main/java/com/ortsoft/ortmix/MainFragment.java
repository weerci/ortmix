package com.ortsoft.ortmix;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.slider.Slider;
import com.ortsoft.ortmix.messages.MessageSend;
import com.ortsoft.ortmix.model.Zone;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainFragment extends Fragment {
    private static final String ARG_ZONE = "zone";
    private Zone mZone;

    private TextView mTextVolume;
    private TextView mTextHigh;
    private TextView mTextMid;
    private TextView mTextLow;


    private MessageHandler mMessageHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mZone = getArguments().getParcelable(ARG_ZONE);

        }
    }

    public static MainFragment newInstance(Zone zone) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_ZONE, zone);
        fragment.setArguments(args);
        return fragment;
    }

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        TextView textZone = (TextView) view.findViewById(R.id.textViewNameZone);
        textZone.setText(mZone.getName());

        //TextView mServerState = (TextView) view.findViewById(R.id.txtServerState);
        Slider slideVolume = (Slider) view.findViewById(R.id.slide_volume);
        slideVolume.setValue(mZone.getVolume());
        Slider slideHigh = (Slider) view.findViewById(R.id.slide_high);
        slideHigh.setValue(mZone.getHeight());
        Slider slideMid = (Slider) view.findViewById(R.id.slide_mid);
        slideMid.setValue(mZone.getMid());
        Slider slideLow = (Slider) view.findViewById(R.id.slide_low);
        slideLow.setValue(mZone.getLow());

        mTextVolume = (TextView) view.findViewById(R.id.text_volume);
        mTextVolume.setText(String.format("%2d%%", mZone.getVolume()));

        mTextHigh = (TextView) view.findViewById(R.id.text_high);
        mTextHigh.setText(String.format("%2d%%", mZone.getHeight()));

        mTextMid = (TextView) view.findViewById(R.id.text_mid);
        mTextMid.setText(String.format("%2d%%", mZone.getMid()));

        mTextLow = (TextView) view.findViewById(R.id.text_low);
        mTextLow.setText(String.format("%2d%%", mZone.getLow()));

        Slider.OnChangeListener sliderChange = new Slider.OnChangeListener() {
            @Override
            public void onValueChange(@NonNull Slider slider, float value, boolean fromUser) {
                switch (slider.getId()) {
                    case R.id.slide_volume:
                        mTextVolume.setText(String.format("%.0f%%", value));
                        break;
                    case R.id.slide_high:
                        mTextHigh.setText(String.format("%.0f%%", value));
                        break;
                    case R.id.slide_mid:
                        mTextMid.setText(String.format("%.0f%%", value));
                        break;
                    case R.id.slide_low:
                        mTextLow.setText(String.format("%.0f%%", value));
                        break;
                }
            }
        };

        slideVolume.addOnChangeListener(sliderChange);
        slideHigh.addOnChangeListener(sliderChange);
        slideMid.addOnChangeListener(sliderChange);
        slideLow.addOnChangeListener(sliderChange);

        Slider.OnSliderTouchListener sliderTouchListener = new Slider.OnSliderTouchListener() {
            @Override
            public void onStartTrackingTouch(@NonNull Slider slider) {
            }

            @Override
            public void onStopTrackingTouch(@NonNull Slider slider) {
                switch (slider.getId()) {
                    case R.id.slide_volume:
                        mZone.setVolume((int) slider.getValue());
                        break;
                    case R.id.slide_high:
                        mZone.setHeight((int) slider.getValue());
                        break;
                    case R.id.slide_mid:
                        mZone.setMid((int) slider.getValue());
                        break;
                    case R.id.slide_low:
                        mZone.setLow((int) slider.getValue());
                        break;
                }
                MessageSend.Param(mZone);
            }
        };

        slideVolume.addOnSliderTouchListener(sliderTouchListener);
        slideHigh.addOnSliderTouchListener(sliderTouchListener);
        slideMid.addOnSliderTouchListener(sliderTouchListener);
        slideLow.addOnSliderTouchListener(sliderTouchListener);

        return view;
    }
}