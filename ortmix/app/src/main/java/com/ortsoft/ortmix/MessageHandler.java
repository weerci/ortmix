package com.ortsoft.ortmix;

import static android.content.ContentValues.TAG;
import static com.ortsoft.ortmix.common.Routine.E_TAG;

import android.util.Log;

import com.ortsoft.ortmix.common.Settings;
import com.ortsoft.ortmix.common.Utils;
import com.ortsoft.ortmix.messages.Message;
import com.ortsoft.ortmix.messages.TypeMessage;
import com.ortsoft.ortmix.net.Broadcast;
import com.ortsoft.ortmix.net.ConnectParam;
import com.ortsoft.ortmix.net.TcpClient;

import java.io.IOException;

public class MessageHandler {

    private Receive mReceive;
    private String mLocalAddress = Utils.getIPAddress(true);
    private boolean isRunning = false;

    public MessageHandler(Receive mReceive) {
        this.mReceive = mReceive;
    }

    public MessageHandler run() {
        Broadcast.INSTANCE.connect(Broadcast.INSTANCE.getmConnectParam(), new Broadcast.Event() {
            @Override
            public void serverConnected(Message message, ConnectParam connectParam) throws IOException {

                Log.i(TAG, "Received package from " + connectParam.getAddress());

                // Если сообщение не от сервера (hello = Settings.BS_HELLO) или свое собественное
                // возвращаем управление
                if (mLocalAddress.equals(connectParam.getAddress()) || message.getHello() != Settings.BS_HELLO) {
                    Log.i(TAG, "The package is own or not authorised " + connectParam.getAddress());
                    return;
                }

                // Уведомляем приложение о broadcast сообщении
                mReceive.receiveMessage(message);
                Log.i(TAG, "Attempt connection to server by tcp/ip " + connectParam.getAddress());

                // Пытаемся установтиь соединение с сервером
                try {
                    TcpClient.INSTANCE.connect(connectParam, new TcpClient.Event() {
                        @Override
                        public void raise(Message message) {
                            if (message.getTypeMessage() == TypeMessage.TCP_CONNECTION_IS_ESTABLISHED){
                                // Соединение установлено, можно закрывать broadcast
                                Broadcast.INSTANCE.stop();
                            }
                        }
                    }).run();
                } catch (Exception e) {
                    Log.e(E_TAG, Log.getStackTraceString(e));
                }
            }
        }).run();
        isRunning = true;
        return this;
    }

    public void stop() {
        try {
            Broadcast.INSTANCE.stop();
        } finally {
            isRunning = false;
        }
    }

    public interface Receive{

        public void receiveMessage(Message message);

    }
}
