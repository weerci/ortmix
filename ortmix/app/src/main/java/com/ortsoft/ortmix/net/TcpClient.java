package com.ortsoft.ortmix.net;

import static android.content.ContentValues.TAG;

import static com.ortsoft.ortmix.common.Routine.E_TAG;

import android.util.Log;

import com.ortsoft.ortmix.common.Settings;
import com.ortsoft.ortmix.messages.Message;
import com.ortsoft.ortmix.messages.TypeMessage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public enum TcpClient {
    INSTANCE;

    private ConnectParam mConnectParam;
    private Socket mSocket;
    private Event mEvent;
    private BufferedReader mBufferIn;
    private boolean mIsRun = false;

    public TcpClient connect(ConnectParam connectParam, Event event){
        this.mConnectParam = connectParam;
        mEvent = event;
        return this;
    }

    public boolean isRun()
    {
        return mIsRun && mSocket != null;
    }

    public ConnectParam getConnectParam() {
        return mConnectParam;
    }

    public TcpClient run() throws Exception {

        try {
            mSocket = new Socket(mConnectParam.getAddress(), Settings.SRV_PORT);
            mIsRun = true;

            Log.i(TAG, "Connected by tcp/ip to server " + mConnectParam.getAddress());
            mEvent.raise(Message.getCommand(TypeMessage.TCP_CONNECTION_IS_ESTABLISHED));

            // Отправляем запрос на синхронизацию
            sendMessage(Message.getCommand(TypeMessage.GET_SYNC));

            new Thread() {
                @Override
                public void run() {
                    super.run();
                    try {
                        mBufferIn = new BufferedReader(new InputStreamReader(mSocket.getInputStream()));
                    } catch (IOException e) {
                        Log.e(E_TAG, Log.getStackTraceString(e));
                    }
                    while (mIsRun) {
                        if (mSocket != null) {
                            try {
                                String mServerMessage = mBufferIn.readLine();
                                mEvent.raise(Message.getReceived(mServerMessage.getBytes(StandardCharsets.US_ASCII)));
                            } catch (IOException e) {
                                mIsRun = false;
                                Log.e(TAG, e.getMessage());
                            }
                        }
                    }
                }
            }.start();

        } catch (Exception e) {
            mIsRun = false;
            Log.e(TAG, "Connected by tcp/ip to server " + mConnectParam.getAddress() + " is filed!");
        };
        return this;
    }

    public void sendMessage(Message message) throws Exception {
        new Thread() {
            @Override
            public void run() {
                super.run();
                if (mIsRun) {
                    try {
                        mSocket.getOutputStream().write(message.getMsg());
                        mSocket.getOutputStream().flush();
                    } catch (Exception e) {
                        Log.e(TAG, e.getMessage());
                    }
                }
            }
        }.start();
    }

    public interface Event{

        public void raise(Message message);

    }
}
