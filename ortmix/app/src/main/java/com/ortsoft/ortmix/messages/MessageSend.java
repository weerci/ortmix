package com.ortsoft.ortmix.messages;

import static com.ortsoft.ortmix.common.Routine.E_TAG;

import android.util.Log;

import com.ortsoft.ortmix.model.Zone;
import com.ortsoft.ortmix.model.Params;
import com.ortsoft.ortmix.net.TcpClient;

/**
 * Инкапсулирет работу по отправке сообщений серверу об изменении значения параметров
 */
public class MessageSend {

    public static void Param(Zone zone) {
        if (TcpClient.INSTANCE.isRun()) {
            try {
                TcpClient.INSTANCE.sendMessage(Message.getParams(TypeMessage.PARAM, zone));
            } catch (Exception e) {
                Log.e(E_TAG, Log.getStackTraceString(e));
            }
        }
    }

}
