package com.ortsoft.ortmix;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.ortsoft.ortmix.model.Zone;

public class ViewPagerFragmentStateAdapter extends FragmentStateAdapter {


    public ViewPagerFragmentStateAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return MainFragment.newInstance(Zone.Zones.get(position));
    }

    @Override
    public int getItemCount() {
        return Zone.Zones.size();
    }
}
