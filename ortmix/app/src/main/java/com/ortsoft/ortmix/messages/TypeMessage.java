package com.ortsoft.ortmix.messages;

// Тип передаваемого сообщения
public enum TypeMessage {

    /** При изменении значений зоны, тип этого сообщения посылается серверу TCP/IP.
     * Этот же тип сообщений посылается при синхронизации, от сервера клиенту. */
    PARAM(10),

    /**Запрос к серверу на синхронизацию*/
    GET_SYNC (11),

    /** Команды протокала*/

    /** Закрытие TCP/IP соединения. */
    CLOSE_CONNECT(20),

    /** TCP/IP соединение с сервером установлено*/
    TCP_CONNECTION_IS_ESTABLISHED(21),

    /** При получении сообщения этого типа, UDP socket на стороне сервера формирует и отвечает
     * клиету сообщением типа FOUND_SERVER. */
    LOOKING_SERVER(22),

    /** Сервер TCP/IP найден и готов к установлению соединения.
     * UDP socket, на стороне сервера формирует сообщение этого типа и отправляет клиенту.
     * Клиент устанавливает TCP/IP соединение и прекращает
     * broadcast рассылку. */
    FOUND_SERVER(23),

    /** Гененрируется клиентом, если не удется соединиться с сервером. */
    FILED_TO_CONNECT(24);


    private final int value;

    private TypeMessage(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
