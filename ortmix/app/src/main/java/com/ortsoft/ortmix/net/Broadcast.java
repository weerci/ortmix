package com.ortsoft.ortmix.net;

import static android.content.ContentValues.TAG;

import static com.ortsoft.ortmix.common.Routine.E_TAG;

import android.util.Log;

import com.ortsoft.ortmix.messages.Message;
import com.ortsoft.ortmix.common.Settings;
import com.ortsoft.ortmix.messages.TypeMessage;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public enum Broadcast {
    INSTANCE;

    private final Object mLock = new Object();
    private boolean mIsActiveSending = false;
    private boolean mIsActiveReceive = false;
    private Event mReceive;
    private DatagramSocket mUdpSocket;
    private ConnectParam mConnectParam = new ConnectParam(Settings.BS_ADDRESS, Settings.BS_PORT);

    public ConnectParam getmConnectParam(){
        return mConnectParam;
    }

    public Broadcast connect(ConnectParam connectParam, Event receive) {
        mReceive = receive;
        mConnectParam = connectParam;
        try {
            mUdpSocket = new DatagramSocket(Settings.BS_PORT);
        } catch (SocketException e) {
            Log.e(E_TAG, Log.getStackTraceString(e));
        }
        return this;
    }

    public Broadcast run()
    {
        StartDataReceive();
        StartDataSending();
        return this;
    }

    public void stop()
    {
        StopDataSending();
        StopDataReceive();
    }

    /**
     * Запускается бесконечный цикл для отправки broadcast сообщения всем в сети.
     * Сообщения отправляются в отдельном потоке
     */
    private void StartDataSending() {
        new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    synchronized (mLock) {
                        mIsActiveSending = true;
                    }

                    Message lookingServer = Message.getCommand(TypeMessage.LOOKING_SERVER);
                    DatagramPacket packHello = new DatagramPacket(
                            lookingServer.getMsg(), lookingServer.getMsg().length,
                            InetAddress.getByName(mConnectParam.getAddress()), mConnectParam.getPort());

                    while (mIsActiveSending) {
                        // Пакеты посылаются до тех пор, пока не будет получен ответ с сервера и установлена коннекция по TCP/IP.
                        // После этого messagesHandler устанавливает занчениме mIsActiveSending = false, и рассылка пакетов прекращается
                        try {
                            mUdpSocket.send(packHello);
                            Log.i(TAG, "Send package type LOOKING_SERVER");
                        } catch (Exception e) {
                            Log.e(E_TAG, Log.getStackTraceString(e));
                        }

                        Thread.sleep(Settings.BS_TIME_INTERVAL);
                    }
                } catch (Exception ex) {
                    Log.e(TAG, ex.getMessage());
                    synchronized (mLock) {
                        mIsActiveSending = false;
                    }
                }
            }
        }.start();
    }

    /**
     * Stop sending broadcast messages
     * */
    private void StopDataSending() {
        synchronized (mLock) {
            mIsActiveSending = false;
        }
    }

    /**
     * Запускается бесконечный цикл для отслеживания сообщений от найденых компьютеров
     * Получение сообщений запускается в отдельном потоке
     * */
    private void StartDataReceive() {
        new Thread() {
            @Override
            public void run() {
                super.run();
                synchronized (mLock) {
                    mIsActiveReceive = true;
                }

                try {
                    while (mIsActiveReceive) {
                        byte[] data = new byte[Settings.BS_ANSWER];
                        DatagramPacket packet = new DatagramPacket(data, data.length);
                        mUdpSocket.receive(packet);

                        mReceive.serverConnected(Message.getCommand(TypeMessage.FOUND_SERVER),
                                new ConnectParam(packet.getAddress().getHostAddress(), Settings.SRV_PORT));
                    }
                } catch (IOException ex) {
                    Log.e(TAG, ex.getMessage());
                } finally {
                    synchronized (mLock) {
                        mIsActiveSending = false;
                    }
                }
            }
        }.start();
    }

    private void StopDataReceive() {
        synchronized (mLock) {
            mIsActiveReceive = false;
        }
    }

    public interface Event
    {
        public void serverConnected(Message message, ConnectParam connectParam) throws IOException;
    }
}
