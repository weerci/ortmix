package com.ortsoft.ortmix.net;

import com.ortsoft.ortmix.common.Settings;

/**
 * Класс содержит имя, адрес и порт соединения
 */
public class ConnectParam {

    private String address;
    private String name;
    private int port;

    public ConnectParam(String address) {
        this.address = address;
    }

    public ConnectParam(String address, int port) {
        this(address);
        this.port = port;
    }

    public ConnectParam(String address, int port, String name) {
        this(address, port);
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public int getPort() {
        return port;
    }
}
